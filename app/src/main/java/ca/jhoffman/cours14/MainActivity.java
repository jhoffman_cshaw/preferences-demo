package ca.jhoffman.cours14;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView wordTextView;
    PreferencesManager preferencesManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferencesManager = new PreferencesManager(this);

        TextView launchesCountTextView = (TextView)findViewById(R.id.activity_main_launches_count_textview);
        int launches = preferencesManager.getLaunchesCount() + 1;
        launchesCountTextView.setText(launches + "");
        preferencesManager.setLaunchesCount(launches);

        wordTextView = (TextView)findViewById(R.id.activity_main_word_textview);
        updateWordTextView();

        findViewById(R.id.activity_main_second_activity_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                i.putExtra(SecondActivity.WORD_EXTRA, preferencesManager.getWord());
                startActivityForResult(i, 100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100 && resultCode == RESULT_OK) {
            String resultString = data.getStringExtra("result_key");
            preferencesManager.setWord(resultString);

            Toast.makeText(this, "Updated preferences: " + resultString, Toast.LENGTH_LONG).show();

            updateWordTextView();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void updateWordTextView() {
        String word = preferencesManager.getWord();

        String wordToDisplay = "[empty]";
        if (word != null && word != "") {
            wordToDisplay = word;
        }
        wordTextView.setText(wordToDisplay);
    }
}
