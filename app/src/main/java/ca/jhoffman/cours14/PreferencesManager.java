package ca.jhoffman.cours14;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jhoffman on 2016-09-25.
 */

public class PreferencesManager {
    private final String PREFERENCES_FILE_NAME = "ca.jhoffman.cours.prefs_file";
    private final String KEY_LAUNCHES_COUNT = "ca.jhoffman.cours.prefs_file.key.launches_count";
    private final String KEY_WORD = "ca.jhoffman.cours.prefs_file.key.word";

    private SharedPreferences preferences;

    PreferencesManager(Context context) {
        preferences = context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    //
    //Launches count
    //

    public int getLaunchesCount() {
        return preferences.getInt(KEY_LAUNCHES_COUNT, 0); //Default is 0
    }

    public void setLaunchesCount(int count) {
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(KEY_LAUNCHES_COUNT, count);
        editor.commit(); // editor.apply() to execute in background
    }

    //
    //Word
    //

    public String getWord() {
        return preferences.getString(KEY_WORD, null); //Default is null
    }

    public void setWord(String word) {
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(KEY_WORD, word);
        editor.commit();
    }
}
