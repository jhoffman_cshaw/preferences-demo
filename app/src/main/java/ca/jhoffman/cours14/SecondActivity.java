package ca.jhoffman.cours14;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    public static final String WORD_EXTRA = "ca.jhoffman.cours.extra.word";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        final EditText editText = (EditText)findViewById(R.id.activity_second_edit_text);
        editText.setText(getIntent().getStringExtra(WORD_EXTRA));

        findViewById(R.id.activity_second_confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();

                Bundle resultData = new Bundle();
                resultData.putString("result_key", text);

                Intent intent = new Intent();
                intent.putExtras(resultData);

                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
